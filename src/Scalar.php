<?php
namespace Scalar;

use Exception;

class Scalar {
  
  /**
   * @var $Scalar
   * Value that only can be scalar.
   */
  public $Scalar;
  
  /**
   * @param $Scalar
   */
  function __construct ($Scalar) {
    
    try{
      if(!is_scalar($Scalar)){
        throw new Exception('It\'s not a Scalar value');
      }else {
        
        $this->Scalar = $Scalar;
      }
    }
    catch(Exception $e) {
      $e->getMessage();
    }
  }
  
  /**
   * @param $method
   * @param null $arguments
   * Array $arguments will be use as an associative array, one of the elements must be ['scalar' => ''], it can be empty, but must be defined.
   * @return mixed
   */
  public function __call($method, $arguments ) {
    
    try{

      if (function_exists($method) == FALSE) {
        throw new Exception ('The function called ' . $method . ' doesn\'t exist');
      } else {
        $arguments['scalar'] = $this->Scalar;
        return call_user_func_array ($method, $arguments[0]);
      }
    }
    catch (Exception $e) {
      $e->getMessage();
    }
  }
}