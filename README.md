Scalar class it's a class that looks only to make the works with function a little be more like OOP.

You can declare a PHP Scalar value as the param of the class

```php
$name = 'Oskar';
$name = new Scalar($name);
``` 

Onces you have declared your Scalar object you can use method on the fly like this:
```php
echo $name->mb_strlen();
// it will return 5
```

You also can use this class with comples functions with two or more arguments, in the case
you want to use the Scalar object and call a method with two or more arguments you must use a
PHP array dictionary, the order of the dictionary elements must be the same that the original function:

```php
$values = [
'arg1' => 'Su nombre es Oskar',
'scalar' => ''
]
echo $name->strpos($values);
// It will return 13
```

```php
$values = [
  'arg1' => 'Matt', 
  'scalar' => '', 
  'arg20' => 'Hello Matt!'
  ];
echo $name->str_replace();
// It will return Hello Oskar
```